furiganify
==========
# Prerequisites
mecab
jaconv (python pypi library)

# Usage
```
Usage:
furiganify [OPTIONS] [FILE]
COMMAND_NAME | furiganify [OPTIONS]

OPTIONS:
    -h, --help
        show this text
    -d, --dicdir <directory>
        use a custom dictionary directory. See \`mecab --help\` for more information.

FILE: 
    An input file, if specified this will be used instead of STDIN
```

a basic example would be:

```bash
echo "この猫ここの猫の子猫この子猫ね" | ./furiganify
# stdout: この<<fg 猫 ねこ>>ここの<<fg 猫 ねこ>>の<<fg 子猫 こねこ>>この<<fg 子猫 こねこ>>ね
```

Here's another example using a file:

```bash
# hayakutikotoba.txt
この猫ここの猫の子猫この子猫ね
生麦生米生卵

# terminal
./furiganify hayakutikotoba.txt
# stdout: 
# この<<fg 猫 ねこ>>ここの<<fg 猫 ねこ>>の<<fg 子猫 こねこ>>この<<fg 子猫 こねこ>>ね
# <<fg 生 せい>><<fg 麦生 むぎう>><<fg 米生 よねお>><<fg 卵 たまご>>
```
The second part of the previous example shows that you have to be careful when using automated tools to help with generating furigana, because they can't always get it right. (proper output would be 「`<<fg 生麦 なまむぎ>><<fg 生米 なまごめ>><<fg なまたまご>>`」)

# COPYRIGHT
The MIT License, you can see it in the [LICENSE] file that was downloaded with this project.

# Future work:
This project doesn't even have a `-v` switch yet, so far to go hopefully. Planned features may include:
* Custom output format options such as anki style (` word[furigana]`), html ruby (`<ruby>word<rp>（</rp><rt>furigana</rt><rp>）</rp></ruby>`) and possibly custom styles using some kind of formatting.
* Better parsing, namely better dictionary files.
