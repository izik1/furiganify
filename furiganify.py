#!/usr/bin/python3
# -*- coding: utf-8 -*-

import re
import jaconv

import unicodedata
import sys
import string

def is_kanji(ch):
    return 'CJK UNIFIED IDEOGRAPH' in unicodedata.name(ch)


def is_hiragana(ch):
    return 'HIRAGANA' in unicodedata.name(ch)

for expr in sys.stdin:
    out = []
    for node in expr.rstrip("\n").split(" "):
        if not node:
            break
        (kanji, reading) = re.match("(.+)\[(.*)\]", node).groups()
        # katakana, punctuation, not japanese, or lacking a reading
        if kanji == reading or not reading or all(is_hiragana(elem) for elem in kanji):
            out.append(kanji)
            continue
        
        # hiragana
        if all(is_hiragana(elem) for elem in kanji):
            out.append(kanji)
            continue

        # ignore ascii alphanumerics
        if all(ch in string.ascii_letters or ch in string.digits for ch in kanji):
            out.append(kanji)
            continue

        # don't add readings of numbers
        if kanji in u"一二三四五六七八九十０１２３４５６７８９":
            out.append(kanji)
            continue


        reading = jaconv.kata2hira(reading)

        # shamelessly taken from the anki plugin 'japanese-support' 
        # strip matching characters and beginning and end of reading and kanji
        # reading should always be at least as long as the kanji
        placeL = 0
        placeR = 0
        for i in range(1,len(kanji)):
            if kanji[-i] != reading[-i]:
                break
            placeR = i
        for i in range(0,len(kanji)-1):
            if kanji[i] != reading[i]:
                break
            placeL = i+1
        if placeL == 0:
            if placeR == 0:
                out.append("<<fg %s %s>>" % (kanji, reading))
            else:
                out.append("<<fg %s %s>>%s" % (
                    kanji[:-placeR], reading[:-placeR], reading[-placeR:]))
        else:
            if placeR == 0:
                out.append("%s<<fg %s %s>>" % (
                    reading[:placeL], kanji[placeL:], reading[placeL:]))
            else:
                out.append("%s<<fg %s %s>>%s" % (
                    reading[:placeL], kanji[placeL:-placeR],
                    reading[placeL:-placeR], reading[-placeR:]))

    print("".join(out))

